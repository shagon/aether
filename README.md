# **aether**
*the clear sky; the upper regions of air beyond the clouds*

**Table of Contents**
- [**aether**](#aether)
	- [Quickstart](#quickstart)
	- [How It Works](#how-it-works)
	- [Config](#config)
	- [Example usage](#example-usage)
	- [Libraries that made this project possible](#libraries-that-made-this-project-possible)

___

**aether** is a DNS server written in golang. It has support for adding your own domain resolvers, as well as serving custom zonefiles, e.g. with for a TLD of `.example`.

* **aether** has a small footprint, can be served on any port as well as specifying any DNS resolver in case it doesn't have the record cached.
* **aether** has a dockerfile in case you would like to try it out, the image size is around 8MB.
* Use cases for this would be to forward DNS traffic if you're behind a VPN so they don't leak.
* Hiding some services by using the aether to serve the domains you need instead of publishing them at your domain registar you can secure specific services.
* Since you can supply your own zonefiles you can create an adblock service simply by feeding the data to the server.
* Almost everything is logged to the standard output (everything except the DNS queries themselves) as that would expand the log size by quite a large amount. 

<p align="center">
  <img src="./img/terminal.png" alt="Size Limit CLI" width="768">
</p>

## Quickstart

If you want to skip ahead and test what this can do you can either build the app:

```bash
git clone https://gitlab.com/shagon/aether.git && cd aether
go get -v && go build
```

Additionally docker containers are also provided. You can find them [here](https://gitlab.com/shagon/aether/container_registry) or just run it as:

```bash
$ docker run -it --rm -p 53:53/udp registry.gitlab.com/shagon/aether:master
```

## How It Works

1. All DNS records are served from the application cache, if you enter a custom zonefile those will have presedence over forwarding the query to another DNS server (supplied as `-forward-server`).
2. If zonefiles are supplied aether checks for the `etag` response header in case the record updates so it can renew the data. If there is no `etag` header currently the cache key will be empty however most webservers already send this header.
3. If started as a service the logging is useful in case you have any errors as well as to manage the server.
4. By default if started without any parameters it will act as a DNS forwarder.

## Config

**aether** supports multiple command line arguments.

* **-forward-server**: the DNS server that aether will contact in case the result is not located in its cache (default "`1.1.1.1:53`"), note: if supplying the `forward-server` include the port, e.g. `8.8.8.8:53`
* **-interface**: the interface to listen to (default "`0.0.0.0`")
* **-port**: the port to use (default "`53`"), if running on linux anything below 1024 are restricted and as such require `root` permissions; you can serve the DNS on non standard ports with this, e.g. on port `5300`
* **-refresh**: default refresh time for the zonefiles, the zonefiles themselves are specified with the `-zone` parameter (default `60` seconds), to specify a higher or lower TTL for the files provided simply enter the time in seconds, example: `-refresh 5` would check for changes every 5 seconds; an example of specifying an hour would be to pass the whole number as `-refresh 3600` or rely on arithmetics, e.g. in bash: `-refresh $((60*60))` is the same as `-refresh 3600`
* **-zone** value RFC1035 (bind9 format) zonefiles to use, an example zonefile would either be complete:

```zone
; example.demo domain name
$TTL 3600
$ORIGIN example.demo.

@    IN A 1.2.3.4
ns   IN A 1.1.1.1
mail IN A 4.3.2.1

@     IN MX 20 cname.example.demo.
@     IN NS ns
@     IN TXT "example txt record"
@     IN SOA ns support 1 10800 3600 604800 3600
cname IN CNAME example.demo.
```

This would return the following when checked:

```zone
$ dig +noall +answer A example.demo @localhost
example.demo.		3600	IN	A	1.2.3.4

$ dig +noall +answer A ns.example.demo @localhost
ns.example.demo.	3600	IN	A	1.1.1.1

$ dig +noall +answer A mail.example.demo @localhost
mail.example.demo.	3600	IN	A	4.3.2.1

$ dig +noall +answer MX example.demo @localhost
example.demo.		3600	IN	MX	20 cname.example.demo.

$ dig +noall +answer NS example.demo @localhost
example.demo.		3600	IN	NS	ns.example.demo.

$ dig +noall +answer TXT example.demo @localhost
example.demo.		3600	IN	TXT	"example txt record"

$ dig +noall +answer SOA example.demo @localhost
example.demo.		3600	IN	SOA	ns.example.demo. support.example.demo. 1 10800 3600 604800 3600

$ dig +noall +answer CNAME cname.example.demo @localhost
cname.example.demo.	3600	IN	CNAME	example.demo.
```

However you can also supply the zones as:

```zone
$TTL 3600
example.com.        IN A 1.2.3.4
random.domain.name. IN A 2.2.2.2
```

Please note the dot at the end of the record, `example.com.`, since we did not specify an `$ORIGIN` we need to add the trailing dot at the end.

This would return the following:

```zone
$ dig +noall +answer example.com @localhost
example.com.  3600	IN	A	1.2.3.4

$ dig +noall +answer random.domain.name @localhost
random.domain.name.  3600	IN	A	2.2.2.2
```

## Example usage

* Start the program with one specific zonefile
```zone
aether -zone https://domain.com/file.zone
```
* Start the program with multiple zonefiles
```zone
aether -zone https://domain.com/file.zone -zone https://example.com/zonefile
```
* Set the TTL / refresh time of the zonefiles to 2 hours
```zone
aether -refresh 7200 -zone https://domain.com/zonefile 
```
* Change the default port and interface
```zone
aether -interface 127.0.0.1 -port 5678
```
* Change the defaults
```zone
aether -interface 127.0.0.1 -port 5353 -refresh 3600 -forward-server 8.8.8.8:53 -zone https://domain.com/zonefile
```

## Libraries that made this project possible

* go.uber.org/zap - logger
* github.com/miekg/dns - main dns library

<a target="_blank" href="https://icons8.com/icons/set/entering-heaven-alive">Heaven icon</a> by <a target="_blank" href="https://icons8.com">Icons8</a>.