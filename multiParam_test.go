package main

import (
	"testing"
)

func TestMulti_String(t *testing.T) {
	tests := []struct {
		name string
		m    *Multi
		want string
	}{
		{"single", &Multi{"something"}, "something"},
		{"multiple", &Multi{"word1", "word2"}, "word1,word2"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.m.String(); got != tt.want {
				t.Errorf("Multi.String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMulti_Set(t *testing.T) {
	type args struct {
		args string
	}
	tests := []struct {
		name    string
		m       *Multi
		args    args
		wantErr bool
	}{
		{"set1", &Multi{"word"}, args{"another"}, false},
		{"set2", &Multi{"word"}, args{"some more"}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.m.Set(tt.args.args); (err != nil) != tt.wantErr {
				t.Errorf("Multi.Set() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
