#!/bin/bash

set -e
declare -a adlists=("https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts" "https://mirror1.malwaredomains.com/files/justdomains")

dl_file="$(mktemp)"
output_file=./adblock.zone

for link in "${adlists[@]}"; do
    curl -sS "$link" >> "$dl_file"
done

cat "$dl_file" \
    | sed '
        # Remove comments
        s/#.*$//g

        # Remove leading spaces
        s/^[ \t]*//

        # Remove trailing spaces
        s/[ \t]*$//

        # Remove DOS newlines
        s/\r//g
    ' \
    | awk '/./ { print $2 }' \
    | grep -v \
        -e '^localhost$' \
        -e '^localhost\.localdomain$' \
        -e '^local$' \
        -e '^broadcasthost$' \
        -e '^0.0.0.0$' \
    | sed 's/$/. IN A 0.0.0.0/' \
    | sort \
    | uniq \
    > "$output_file"

chmod 644 "$output_file"
rm "$dl_file"
