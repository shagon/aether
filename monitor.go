package main

import (
	"net/http"
	"time"
)

// monitor checks for updates, if a new etag has occurred every DNS record from the zones is recreated to accommodate the change
func monitor() {
	etags := make(map[string]string)
	for range time.Tick(time.Second * time.Duration(*refresh)) {
		for _, zurl := range *zones {
			resp, err := http.Head(zurl)
			if err != nil {
				logger.Sync()
				sugar.Fatal(err)
			}
			defer resp.Body.Close()

			et := resp.Header.Get("etag")
			zn := getZoneName(zurl)
			ot, exists := etags[zn]

			if !exists {
				etags[zn] = et
				sugar.Debugf("cached %s with etag %s", zn, et)
			}
			if exists && et != ot { // if updated
				etags[zn] = et
				sugar.Debugf("zone %s has new etag %s updated to %s", zn, et, ot)
				go createRecords()
			}
		}
	}
}
