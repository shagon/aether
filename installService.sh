#!/bin/bash
if [ "$EUID" -ne 0 ]; then
    echo "You need to run this script as root"
    exit 1
fi

mkdir -p ~/go/src/gitlab.com/shagon
git clone git@gitlab.com:shagon/aether ~/go/src/gitlab.com/shagon/aether
cd ~/go/src/gitlab.com/shagon/aether
go get -v
sudo -u root go build -o /usr/local/bin/aether
sudo -u root cp aether.service /etc/systemd/system/aether.service
sudo -u root systemctl daemon-reload
sudo -u root systemctl start aether
sudo -u root systemctl status aether
sudo -u root systemctl enable aether
