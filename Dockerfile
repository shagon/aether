FROM golang:alpine as build

WORKDIR /go/src/gitlab.com/shagon/aether
EXPOSE 53/udp
COPY . ./

RUN apk add --no-cache git \
    && go get -d -v ./... \
    && go install .

FROM alpine as run
RUN apk add --no-cache ca-certificates

COPY --from=build /go/bin/aether /go/bin/aether
CMD [ "/go/bin/aether" ]
ENTRYPOINT [ "/go/bin/aether" ]