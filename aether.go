package main

import (
	"bufio"
	"flag"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"path"
	"syscall"

	"github.com/miekg/dns"
	"go.uber.org/zap"
)

var (
	forwardServer   = flag.String("forward-server", "1.1.1.1:53", "forward DNS server")
	listenInterface = flag.String("interface", "0.0.0.0", "interface to listen to (requires elevated permissions)")
	port            = flag.String("port", "53", "port to use")
	zones           = Flag("zone", "RFC1035 (bind9 format) zonefiles to use")
	refresh         = flag.Int("refresh", 60, "default refresh time for the zonefiles")
	logger          = &zap.Logger{}
	sugar           = &zap.SugaredLogger{}
	rrs             = []dns.RR{}
	srv             = &dns.Server{}
	sp              = NewSemaphore(1)
)

func main() {
	flag.Parse()
	loggerConfig := zap.NewDevelopmentConfig()
	loggerConfig.DisableStacktrace = true
	loggerConfig.EncoderConfig.TimeKey = "" // hide time from output
	loggerConfig.DisableCaller = true       // hide filename that called
	logger, _ = loggerConfig.Build()
	sugar = logger.Sugar()

	if flag.Lookup("test.v") != nil { // turn off logging for tests
		logger = zap.NewNop()
		sugar = logger.Sugar()
	}

	sugar.Debugf("forward-server set to %s", *forwardServer)
	sugar.Debugf("interface set to %s", *listenInterface)
	sugar.Debugf("port set to %s", *port)
	sugar.Debugf("refresh set to %d seconds", *refresh)

	dns.HandleFunc(".", handleDNS)
	defer logger.Sync()

	if len(*zones) != 0 { // if empty just proxy requests
		createRecords()
		go monitor()
	}

	sugar.Info("Starting service")
	go startService()

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	sugar.Warnf("Signal (%v) received, stopping", <-sig)
	if err := srv.Shutdown(); err != nil {
		sugar.Errorf("failed to shutdown %s", err)
	}
}

func resolver(forwardServer, fqdn string, rType uint16) []dns.RR {
	m := new(dns.Msg)
	m.Id = dns.Id()
	m.SetQuestion(fqdn, rType)

	in, err := dns.Exchange(m, forwardServer)
	if err == nil {
		return in.Answer
	}
	return []dns.RR{}
}

func startService() {
	addr := fmt.Sprintf("%s:%s", *listenInterface, *port) // IP:PORT
	srv.Addr = addr
	srv.Net = "udp"
	err := srv.ListenAndServe()
	if err != nil {
		sugar.Fatalf("Failed to start server on %s - %s", addr, err.Error())
	}
}

func handleDNS(rw dns.ResponseWriter, r *dns.Msg) {
	m := new(dns.Msg)
	m.SetReply(r)
	m.Authoritative = true

	for _, q := range r.Question {
		answers := []dns.RR{}
		for _, rr := range rrs { // resource record, check if in cache
			rh := rr.Header()

			if rh.Rrtype == dns.TypeCNAME && q.Name == rh.Name {
				answers = append(answers, rr)

				location := fmt.Sprintf("%s:%s", *listenInterface, *port) // IP:PORT
				for _, a := range resolver(location, rr.(*dns.CNAME).Target, q.Qtype) {
					answers = append(answers, a)
				}
			}

			if q.Name == rh.Name && q.Qtype == rh.Rrtype && q.Qclass == rh.Class {
				answers = append(answers, rr)
			}
		}

		if len(answers) == 0 && *forwardServer != "" {
			for _, a := range resolver(*forwardServer, q.Name, q.Qtype) {
				answers = append(answers, a)
			}
		}
		for _, a := range answers {
			m.Answer = append(m.Answer, a)
		}
	}
	rw.WriteMsg(m)
}

func createRecords() {
	<-sp.Wait()
	rrsSize := len(rrs)
	for _, zurl := range *zones {
		sugar.Debugf("parsing zone %s", zurl)
		resp, err := http.Get(zurl)
		if err != nil {
			logger.Sync()
			sugar.Fatal(err)
		}
		defer resp.Body.Close()

		reader := bufio.NewReaderSize(resp.Body, 2048)
		zn := getZoneName(zurl)
		p := dns.NewZoneParser(reader, "", zurl)
		var i int
		for rr, ok := p.Next(); ok; rr, ok = p.Next() {
			rrs = append(rrs, rr)
			i++
		}

		if p.Err() != nil {
			logger.Sync()
			sugar.Fatal(p.Err())
		}

		sugar.Infof("imported %d records from %s", i, zn)
	}
	rrs = rrs[rrsSize:]
	m := make(map[string]dns.RR)
	rrs = dns.Dedup(rrs, m)
	sugar.Infof("imported %d records in total", len(rrs))
	sp.Signal()
}

// getZoneName - extracts the filename, example.zone becomes example
func getZoneName(zurl string) string {
	u, _ := url.Parse(zurl)
	return path.Base(u.Path)
}
