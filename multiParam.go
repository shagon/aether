package main

import (
	"flag"
	"strings"
)

// Multi - for multiple command line arguments of the same type, e.g. -url 1, -url 2
type Multi []string

// Set for implementing the flag interface
func (m *Multi) Set(args string) error {
	*m = append(*m, strings.TrimSpace(args))
	return nil
}

func (m *Multi) String() string {
	return strings.Join(*m, ",")
}

// Flag to implement multi flag
func Flag(name string, usage string) *Multi {
	var ref Multi
	flag.Var(&ref, name, usage) // https://golang.org/pkg/flag/#hdr-Usage
	return &ref
}
